from selenium import webdriver
import pytest
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
import time


@pytest.fixture()
def driver():
    driver = webdriver.Chrome()
    yield driver
    driver.quit()

def test_slider (driver):
    driver.get("https://demoqa.com/slider")
    slider = driver.find_element(By.CSS_SELECTOR, ".range-slider.range-slider--primary")
    min_number = int(slider.get_attribute("min"))
    max_number = int(slider.get_attribute("max"))
    defauld_vulue = int(slider.get_attribute("value"))
    for i in range(min_number + defauld_vulue, max_number - 20):
        slider.send_keys(Keys.RIGHT)
    assert driver.find_element(By.ID, "sliderValue").get_attribute("value") == "80"
    time.sleep(5)

def test_date_pick (driver):
    driver.get("https://demoqa.com/date-picker")
    date = driver.find_element(By.ID, ("datePickerMonthYearInput"))
    date.click()

    month = driver.find_element(By.CSS_SELECTOR, (".react-datepicker__month-select"))
    month.click()
    select_month = Select(month)
    select_month.select_by_visible_text("June")

    year = driver.find_element(By.CSS_SELECTOR, (".react-datepicker__year-select"))
    year.click()
    select_year = Select(year)
    select_year.select_by_visible_text("2021")

    date = "08"
    tanggal = driver.find_element(By.CSS_SELECTOR, '.react-datepicker__day.react-datepicker__day--0'+date)
    # driver.execute_script("arguments[0].click();", tanggal)
    tanggal.click()

    WebDriverWait(driver, 50000).until(EC.visibility_of_element_located((By.ID, "datePickerMonthYearInput")))
    value = driver.find_element(By.ID, ("datePickerMonthYearInput"))
    done = value.get_attribute("value")
    assert done == "06/23/2021"
    time.sleep(2)
    
def test_datepicker(driver):
    driver.get("https://demoqa.com/date-picker")
    date_input = driver.find_element(By.XPATH, '//*[@id="datePickerMonthYearInput"]')
    date_input.send_keys('03/01/2021')
    WebDriverWait(driver, 50000).until(EC.visibility_of_element_located((By.ID, "datePickerMonthYearInput")))
    value = driver.find_element(By.ID, ("datePickerMonthYearInput"))
    done = value.get_attribute("value")
    assert done == "03/01/2021"
    time.sleep(10)

def test_multiple(driver):
    driver.get("https://demoqa.com/auto-complete")
    input = driver.find_element(By.ID, "autoCompleteMultipleInput")
    input.send_keys("yellow")
    input.send_keys(Keys.DOWN)
    input.send_keys(Keys.ENTER)
    input.send_keys("red")
    input.send_keys(Keys.DOWN)
    input.send_keys(Keys.ENTER)
    input.send_keys("blue")
    input.send_keys(Keys.DOWN)
    input.send_keys(Keys.ENTER)
    time.sleep(10)
    # assert WebDriverWait(driver, 3000).until(EC.presence_of_element_located(
    #     (By.ID, "css-12jo7m5 auto-complete__multi-value__label"))).text == "Yellow"
    # time.sleep(10)