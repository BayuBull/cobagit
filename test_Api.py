from selenium import webdriver
import pytest
import requests
from assertpy import assert_that
import allure


def test_get_all():
    url = 'https://airportgap.dev-tester.com/api/airports/'
    response = requests.get(url)
    assert_that(response.status_code).is_equal_to(200)
    assert len(response.json().get('data')) == 30
def test_get_with_id ():
    param = 'MAG'
    url = f'https://airportgap.dev-tester.com/api/airports/{param}'
    response = requests.get(url)
    assert_that(response.status_code).is_equal_to(200)
    data = response.json().get('data')
    assert_that(data['id']).is_equal_to("MAG")
    assert_that(data['attributes']['iata']).is_equal_to("MAG")
    print(data)